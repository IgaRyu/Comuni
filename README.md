# Comuni
 LiveCode - Ricerca nel databse Comuni delle Poste italiane; Versione per Mac


---

## Origine del progetto.

Questo programma è stato un po’ un *passo in più* rispetto al primo programma creato con LiveCode, ossia CercaDivisioni. Nel primo programma c’era solo un campo su cui fare interrogazioni, per cui tuto sommato la cosa era molto basilare.
In questo caso, invece, avendo un DB molto più articolato ho potuto sperimentare l’uso di selettori e fare query in base alle richieste dell’utente, quindi per:

- C.A.P.
- Comune
- Provincia
- Regione 
- Prefisso Telefonico di Area

Manca un solo campo che avrei potuto inserire nelle possibilità di selezione chiave ossia il codice ISTAT, ma mi pareva una ricerca troppo specifica.

Di nuovo una sola finestra in cui è compreso tutto sin dall’apertura: 

- una frase descrittiva di cosa si può cercare
- un campo in cui fare input
- il solito tasto CERCA
- La pulsantiera per selezione di che colonna usare per fare la ricerca, 
- La griglia che la griglia l’output per il risultato della query.
- Pulsante ESCI

## Sviluppo sorgente

Ho iniziato con lo sviluppo per la versione MacOSX, e non ho trovato particolari difficoltà nel creare il sorgente ed ottenere un eseguibile funzionante praticamente da subito. 

In fondo, ovviamente, c’è un pulsante **Esci** che chiude il database correttamente, prima di sganciare la finestra dall’ambiente grafico e pulire la memoria, restituendo poi il controllo all’utente.

## Note 
Probelmi in versione Windows [^Il problema si è presentato quando ho cercato di ottenere gli stessi risultati, dallo stesso sorgente, in Windows, ma un rapporto, approfondito, delle problematiche sono presenti nel file Readme.md della corrispondete cartella della versione Windows.]

Cartella SqLite [^Esiste una cartella **SqLite** che contiene il file originale di testa da cui ho creato il database sqlite, un file di creazione della struttura del DB sia in formato dump di SqLite (.MTQS) che in formato SQL Ansi (.sql)]
\
\
\
\
\
\

---

#### Ultima revisione file Readme.md 25/11/2018

